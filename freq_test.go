/*
	Copyright 2020 Christopher Creager

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

// freq_test.go: Tests for the main implementation

package freq

import (
	"testing"
	"time"
)

func TestInitNil(t *testing.T) {
	f := Init(10)
	if f == nil {
		t.Fail()
	}
}

func TestSimpleEvents(t *testing.T) {
	var e EventCount
	for i := 0; i < 10; i++ {
		e.append(windowelem{t: (time.Time{}).Add(time.Duration(i) * time.Millisecond), n: 1})
	}
	f := e.Frequency()
	t.Log(f)
	if f < 1000-.1 || f > 1000+.1 {
		t.Fail()
	}
}
