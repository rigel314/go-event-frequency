/*
	Copyright 2020 Christopher Creager

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

// freq_examples_test.go: Example of how to use and test showing frequency measurements

package freq_test

import (
	"fmt"
	"testing"
	"time"

	freq "gitlab.com/rigel314/go-event-frequency"
)

var testCh chan []byte

func TestExample(test *testing.T) {
	testCh = make(chan []byte)
	go func() {
		for i := 0; i < 20; i++ {
			testCh <- []byte{1, 2, 3, 4, 5}
			time.Sleep(10 * time.Millisecond)
		}
	}()

	packetfreq := freq.Init(5)
	bytefreq := freq.Init(5)

	t := time.NewTimer(time.Millisecond * 100)
	i := 0
	for {
		select {
		case v := <-testCh:
			packetfreq.Event()
			bytefreq.EventMulti(len(v))
			if !t.Stop() {
				<-t.C
			}
		case <-t.C:
			packetfreq.NoEvent()
			bytefreq.NoEvent()
		}
		fmt.Printf("packets: %.2f Hz, ", packetfreq.Frequency())
		fmt.Printf("bytes: %.2f Hz\n", bytefreq.Frequency())
		if packetfreq.Frequency() == 0 && i > 0 {
			break
		}
		t.Reset(time.Millisecond * 100)
		i++
	}
}
