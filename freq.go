/*
	Copyright 2020 Christopher Creager

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

// freq.go: Main implementation of the frequency functions

// Package freq implements a frequency counter for periodic events.
package freq

import (
	"container/ring"
	"time"
)

// DefaultWindowSize is the size of a zero-valued EventCount window.
const DefaultWindowSize = 10

// EventCount keeps track of timestamps and event counts.
// The zero value is safe to use, but will have the DefaultWindowSize.
type EventCount struct {
	window *ring.Ring
}

// Init creates an EventCount with the given windowsize.
// Init will not return nil.
func Init(windowsize int) *EventCount {
	return &EventCount{window: ring.New(windowsize)}
}

func (ec *EventCount) check() {
	if ec.window == nil {
		ec.window = ring.New(DefaultWindowSize)
	}
}

func (ec *EventCount) append(e windowelem) {
	ec.check()
	ec.window.Value = e
	ec.window = ec.window.Next()
}
func (ec *EventCount) appendDefault(count int) {
	ec.append(newWindowElem(count))
}

// Event adds one event at the current time to the window and expires the oldest item.
// Returns the frequency of the EventCount.
func (ec *EventCount) Event() float64 {
	ec.appendDefault(1)
	return ec.Frequency()
}

// NoEvent expires the oldest item in the window, same as EventMulti(0).
// Returns the frequency of the EventCount.
func (ec *EventCount) NoEvent() float64 {
	ec.appendDefault(0)
	return ec.Frequency()
}

// EventMulti adds count events at the current time to the window and expires the oldest item.
// Returns the frequency of the EventCount.
func (ec *EventCount) EventMulti(count int) float64 {
	ec.appendDefault(count)
	return ec.Frequency()
}

type windowelem struct {
	t time.Time
	n int
}

func newWindowElem(count int) windowelem {
	return windowelem{t: time.Now(), n: count}
}

// Frequency returns the current frequency of the events without modifying the window.
func (ec *EventCount) Frequency() float64 {
	w := ec.window
	sum := 0
	var firstTime, lastTime time.Time
	var seenFirst bool
	var lastVal int
	var count int
	w.Do(func(i interface{}) {
		if i != nil {
			count++
			elem := i.(windowelem)
			if !seenFirst {
				firstTime = elem.t
				seenFirst = true
			}
			sum += elem.n
			lastTime = elem.t
			lastVal = elem.n
		}
	})
	if count == 0 || count == 1 {
		return 0
	}
	return float64(sum-lastVal) / lastTime.Sub(firstTime).Seconds()
}
